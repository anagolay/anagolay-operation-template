# {{operation_name}}

`{{operation_name}}` is an Anagolay Operation that provides access to a remote file given its URL. It performs an HTTP request to fetch the file and provides its bytes as output.

## Installation

In the Cargo.toml file of your Workflow, specify a dependency toward **{{operation_name}}** crate:

```toml
[dependencies]
{{operation_name}} = {
  git = "https://gitlab.com/anagolay/{{operation_name}}"
  rev = "5f40785b629f83f9c0fa77c733f42d6c093b418"
}
```

Note that the usage of this crate in a `no-std` environment is not possible.

## Features

-   **anagolay** Used to build this crate as dependency for other operations (default)
-   **wasm** Used to build the web assembly bindings (default)
-   **debug_assertions** Used to produce helpful debug messages (default)

## Usage

### Manifest generation

```shell
makers manifest
```

### Nodejs

Since WASM will use [Fetch API](https://developer.mozilla.org/en-US/docs/Web/API/Fetch_API)
in order to perform the HTTP request, it's necessary to polyfill by mean of `node-fetch` or similar:

```javascript
// Polyfill fetch
import fetch from "node-fetch";
import { Headers, Request, Response } from "node-fetch";
global.fetch = fetch;
global.Headers = Headers;
global.Request = Request;
global.Response = Response;
```

## Development

It is suggested to use the Vscode `.devcontainer` or any other sandboxed envirnoment like Gitpod beacuse the Operations must be developed under the same enviroment as they are published.

Git hooks will be initialized on first build or a test. If you wish to init them before run this in your terminal:

```shell
rusty-hook init
```

To help you with writing consitent code we provide you with following:

-   `.gitlab-ci.yml` for your testing and fmt checks
-   `rusty-hook.toml` for the `pre-push` fmt check so your CI is green.

## License

[![License](https://img.shields.io/badge/License-Apache_2.0-blue.svg)](https://opensource.org/licenses/Apache-2.0)
